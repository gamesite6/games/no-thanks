import path from "path";
import { defineConfig } from "vite";
import { svelte } from "@sveltejs/vite-plugin-svelte";

// https://vitejs.dev/config/
export default defineConfig({
  base: "/static/no-thank-you/",
  build: {
    emptyOutDir: false,
    lib: {
      entry: path.resolve(__dirname, "src", "main.ts"),
      fileName: "no-thank-you",
      name: "Gamesite6_NoThankYou",
    },
  },
  css: {
    modules: {},
  },
  plugins: [svelte()],
});
