type Card = number;

type PlayerId = number;

type GameSettings = { playerCounts: number[] };

type GameState = {
  players: Player[];
  deck: Card[];
  card: Card | null;
  chips: number;
  phase: Phase;
};

type Player = {
  id: PlayerId;
  cards: Card[];
  chips: number;
  speech: Speech | null;
};

type Phase =
  | { t: "Choose"; playerId: PlayerId }
  | { t: "Draw"; playerId: PlayerId }
  | { t: "GameComplete" };

type Speech = { t: "No" } | { t: "Yes"; card: Card; chips: number };

type Action = "Take" | "Pass" | "Draw";
