export const defaultSettings: GameSettings = {
  playerCounts: [3, 4, 5, 6, 7],
};

const validPlayerCounts = [3, 4, 5, 6, 7];

export function playerCounts(settings: GameSettings): number[] {
  return validPlayerCounts.filter((c) => settings.playerCounts.includes(c));
}
