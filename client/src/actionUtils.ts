import { createEventDispatcher } from "svelte";

export function createActionDispatcher(): (action: Action) => void {
  const dispatch = createEventDispatcher();

  return (action: Action) => {
    dispatch("action", action);
  };
}

export function isActing(phase: Phase, playerId: PlayerId): boolean {
  switch (phase.t) {
    case "Draw":
      return phase.playerId === playerId;
    case "Choose":
      return phase.playerId === playerId;
    case "GameComplete":
      return false;
  }
}
