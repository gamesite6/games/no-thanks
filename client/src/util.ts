export function groupAdjacent<T>(
  arr: T[],
  isEqual: (a: T, b: T) => boolean = (a, b) => a === b
): T[][] {
  if (arr.length > 0) {
    let previousValue = arr[0];
    let currentArray = [previousValue];
    const result: T[][] = [currentArray];

    for (let i = 1; i < arr.length; i++) {
      const currentValue = arr[i];
      if (isEqual(previousValue, currentValue)) {
        previousValue = currentValue;
        currentArray.push(currentValue);
      } else {
        previousValue = currentValue;
        currentArray = [previousValue];
        result.push(currentArray);
      }
    }
    return result;
  } else {
    return [];
  }
}

export function orderPlayers(players: Player[], user?: PlayerId): Player[] {
  const userIdx = players.findIndex((p) => p.id === user);
  if (userIdx !== -1) {
    return [...players.slice(userIdx), ...players.slice(0, userIdx)];
  } else {
    return players;
  }
}

export function sortAscending(arr: number[]): void {
  arr.sort((a, b) => a - b);
}

export function sortDescending(arr: number[]): void {
  arr.sort((a, b) => b - a);
}
