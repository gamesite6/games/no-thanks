use std::collections::HashSet;

use serde::{Deserialize, Serialize};
use typescript_definitions::TypeScriptify;

#[derive(Debug, Clone, Deserialize, Serialize, TypeScriptify)]
#[serde(rename_all = "camelCase")]
pub struct GameSettings {
    pub player_counts: HashSet<usize>,
}
