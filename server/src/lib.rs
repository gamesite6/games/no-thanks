use enum_iterator::IntoEnumIterator;
use rand::prelude::*;
use serde::{Deserialize, Serialize};
use std::{
    collections::{btree_set::BTreeSet, HashSet},
    iter::FromIterator,
};
use typescript_definitions::TypeScriptify;

pub use settings::GameSettings;

pub mod settings;

pub type PlayerId = u32;
pub type Card = u8;

#[derive(Debug, Clone, Serialize, Deserialize, TypeScriptify)]
#[serde(rename_all = "camelCase")]
pub struct GameState {
    pub players: Vec<Player>,
    pub deck: Vec<Card>,
    pub card: Option<Card>,
    pub chips: u8,
    pub phase: Phase,
}

#[derive(Debug, Clone, Serialize, Deserialize, TypeScriptify)]
#[serde(rename_all = "camelCase")]
pub struct Player {
    pub id: PlayerId,
    pub cards: BTreeSet<Card>,
    pub chips: u8,
    pub speech: Option<Speech>,
}

#[derive(Debug, Clone, Serialize, Deserialize, Eq, PartialEq, TypeScriptify)]
#[serde(tag = "t")]
pub enum Phase {
    #[serde(rename_all = "camelCase")]
    Choose {
        player_id: PlayerId,
    },
    #[serde(rename_all = "camelCase")]
    Draw {
        player_id: PlayerId,
    },
    GameComplete,
}

#[derive(Debug, Clone, Serialize, Deserialize, Eq, PartialEq, TypeScriptify)]
#[serde(tag = "t")]
pub enum Speech {
    No,

    #[serde(rename_all = "camelCase")]
    Yes {
        card: Card,
        chips: u8,
    },
}

#[derive(Debug, Clone, IntoEnumIterator, Serialize, Deserialize)]
pub enum Action {
    Take,
    Pass,
    Draw,
}

impl Action {
    fn is_allowed(&self, actor: &Player, phase: &Phase) -> bool {
        match (phase, self) {
            (Phase::Choose { player_id }, Self::Take) => actor.id == *player_id,
            (Phase::Choose { player_id }, Self::Pass) => actor.id == *player_id && actor.chips > 0,
            (Phase::Choose { .. }, _) => false,

            (Phase::Draw { player_id }, Self::Draw) => actor.id == *player_id,
            (Phase::Draw { .. }, _) => false,

            (Phase::GameComplete, _) => false,
        }
    }
}

pub fn player_counts(settings: &GameSettings) -> Vec<usize> {
    let valid_player_counts: HashSet<usize> = HashSet::from_iter([3, 4, 5, 6, 7]);

    let mut counts: Vec<usize> = settings
        .player_counts
        .intersection(&valid_player_counts)
        .copied()
        .collect();

    counts.sort_unstable();
    counts
}

impl GameState {
    pub fn create(players: &[PlayerId], settings: &GameSettings, seed: i64) -> Option<Self> {
        if !player_counts(&settings).contains(&players.len()) {
            return None;
        }

        let mut rng = rand_pcg::Pcg32::seed_from_u64(seed.unsigned_abs());

        let chips_per_player: u8 = match players.len() {
            n if n < 6 => 11,
            6 => 9,
            7 => 7,
            _ => return None,
        };

        let players = players
            .iter()
            .map(|&player_id| Player {
                id: player_id,
                cards: BTreeSet::new(),
                chips: chips_per_player,
                speech: None,
            })
            .collect::<Vec<_>>();

        let mut deck: Vec<Card> = (3..=35).choose_multiple(&mut rng, 24);
        deck.shuffle(&mut rng);

        let phase = Phase::Draw {
            player_id: players.choose(&mut rng)?.id,
        };

        Some(Self {
            players,
            deck,
            card: None,
            chips: 0,
            phase,
        })
    }

    pub fn perform_action(&self, action: &Action, performed_by: PlayerId) -> Option<Self> {
        let actor = self
            .players
            .iter()
            .find(|player| player.id == performed_by)?;
        if !action.is_allowed(actor, &self.phase) {
            return None;
        }

        let mut next_state = self.clone();

        match (&self.phase, action) {
            (Phase::Choose { player_id }, Action::Take) => {
                next_state.clear_all_speech();
                let card = next_state.card.take()?;
                let current_chips = next_state.chips;

                let actor_mut = next_state.get_player_mut(player_id)?;
                actor_mut.speech = Some(Speech::Yes {
                    card,
                    chips: current_chips,
                });
                actor_mut.cards.insert(card);

                actor_mut.chips += current_chips;
                next_state.chips = 0;

                if next_state.deck.is_empty() {
                    next_state.clear_all_speech();
                    next_state.phase = Phase::GameComplete;
                } else {
                    next_state.phase = Phase::Draw {
                        player_id: *player_id,
                    }
                }
            }
            (Phase::Choose { player_id }, Action::Pass) => {
                next_state.clear_all_speech();
                let actor_mut = next_state.get_player_mut(player_id)?;
                actor_mut.speech = Some(Speech::No);
                actor_mut.chips -= 1;
                next_state.chips += 1;

                let next_actor = next_state.get_next_player(player_id)?;
                next_state.phase = Phase::Choose {
                    player_id: next_actor.id,
                }
            }
            (Phase::Choose { .. }, _) => return None,

            (Phase::Draw { player_id }, Action::Draw) => {
                next_state.card = next_state.deck.pop();
                match next_state.card {
                    Some(_) => {
                        next_state.phase = Phase::Choose {
                            player_id: *player_id,
                        }
                    }
                    None => return None,
                }
            }
            (Phase::Draw { .. }, _) => return None,

            (Phase::GameComplete, _) => return None,
        }

        Some(next_state)
    }
    fn clear_all_speech(&mut self) {
        for player in self.players.iter_mut() {
            let _ = player.speech.take();
        }
    }
    fn get_player_mut(&mut self, id: &PlayerId) -> Option<&mut Player> {
        self.players.iter_mut().find(|player| player.id == *id)
    }
    fn get_next_player(&self, player_id: &PlayerId) -> Option<&Player> {
        let idx = self.players.iter().position(|p| p.id == *player_id)?;
        let next_idx = (idx + 1) % self.players.len();
        self.players.get(next_idx)
    }

    pub fn is_game_completed(&self) -> bool {
        matches!(self.phase, Phase::GameComplete)
    }
}
