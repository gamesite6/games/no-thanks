use enum_iterator::IntoEnumIterator;

fn main() {
    #[cfg(debug_assertions)]
    {
        use typescript_definitions::TypeScriptifyTrait;

        println!("type Card = number;\n");
        println!("type PlayerId = number;\n");
        println!("{}\n", lib::GameSettings::type_script_ify());
        println!("{}\n", lib::GameState::type_script_ify());
        println!("{}\n", lib::Player::type_script_ify());
        println!("{}\n", lib::Phase::type_script_ify());
        println!("{}\n", lib::Speech::type_script_ify());

        println!("type Action = ");
        for action in lib::Action::into_enum_iter() {
            println!("  | {}", serde_json::to_value(action).unwrap())
        }
    }
}
