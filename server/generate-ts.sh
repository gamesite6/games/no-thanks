#!/bin/bash
set -e

echo "Generating typescript file ..."
cargo run --bin generate-ts \
  | sed --expression 's/export type/type/g' \
  > ../client/server.d.ts

echo "Type checking generated file ..."
(cd ../client && npx tsc --noEmit ./server.d.ts)

echo "Formatting ..."
(cd ../client && npx prettier --write --loglevel=warn ./server.d.ts)

echo "Done!"
